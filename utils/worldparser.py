# -- coding: utf-8 --**
import defusedxml.ElementTree as ET
from kinematics import Kinematics
import os
DIRPATH = os.path.dirname(__file__)
MANOHANDPATH = os.path.join(os.path.dirname(DIRPATH),"ManoHand")
XMLPATH = os.path.join(os.path.dirname(DIRPATH),"xml")

class WorldParser():
    def __init__(self,basefile=MANOHANDPATH) :
        self._converter = Kinematics(basefile)
        self._world = {}
    def parse(self,filename):
        tree = ET.parse(filename)
        world = tree.getroot()
        robots = tree.findall("robot")
        obj = tree.find("graspableBody")
        obj_transform = list(map(float,obj.find("transform").find("fullTransform").text.replace("("," ").replace(")"," ").replace("["," ").replace("]"," ").strip().split()))
        obj_xyz = obj_transform[-3:]
        obj_quat = obj_transform[:4]
        # TODO
        # trans = self._converter.getPose(obj_xyz,obj_quat)
        # print("Trans : {}".format(trans))
        self._world["object"] = [obj_xyz,obj_quat]
        self._world["robots"]=  []
        for index,robot in enumerate(robots):
            robot_dofs = list(map(float,robot.find("dofValues").text.strip().split()))
            robot_transform = list(map(float,robot.find("transform").find("fullTransform").text.replace("("," ").replace(")"," ").replace("["," ").replace("]"," ").strip().split()))
            robot_xyz =robot_transform[-3:]
            robot_quat = robot_transform[:4]
            trans,pose =self._converter.getManoPose(robot_xyz,robot_quat,robot_dofs)
            self._world["robots"].append([trans,pose])
            self._world["robots"].append([robot_xyz,robot_quat,robot_dofs])
        return self._world

if __name__ == "__main__":
    parser = WorldParser()
    world = parser.parse(os.path.join(XMLPATH,"ori.xml"))
    print(world)
