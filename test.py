import gym
import pybullet as pb
from env.hand_env import HandEnv
from utils.worldparser import WorldParser


if __name__=="__main__":
    import time
    #env = HandEnv(HandEnv.HAND_BOTH)
    parser = WorldParser()
    robots_states=  parser.parse("./xml/ori.xml")
    robots_states[0][0] = [0,0,0]
    env = HandEnv(1)
    env.show_window(True)
    # env.seed(0)

    env.reset(initial_hands_state=robots_states)
    print(robots_states)
    env._client.setRealTimeSimulation(True)


    try:
        while env._client.isConnected():
            time.sleep(0.1)
    except pb.error as err:
        if str(err) not in ['Not connected to physics server.', 'Failed to read parameter.']:
            raise
