import argparse
import time
import numpy as np
import pybullet as p
from env.json_player import JSONPlayer
from env.hand_lift_env import HandLiftEnv





def main(config):
    env = HandLiftEnv(hand_side=1,control_mode=2)
    env = JSONPlayer(env,"./json/lift_duck3.json")
    if config.show_window:
        env.show_window(True)
    else:
        env.show_window(False)
    # env.show_window(False)
    env.reset()
    try:
        while True:
            _observation, _reward, done, _info = env.step()
            time.sleep(0.5)
            if done:
                break
    except RuntimeError:
        print("Done")


if __name__ == "__main__":
    parser  = argparse.ArgumentParser()
    parser.add_argument("--show_window",default=True,type=bool)
    config = parser.parse_args()
    main(config)
